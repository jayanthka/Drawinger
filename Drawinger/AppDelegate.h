//
//  AppDelegate.h
//  Drawinger
//
//  Created by Jeff Menter on 4/18/18.
//  Copyright © 2018 jeffmenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

